\chapter{Introduction}
The use of video-based surveillance system has exploded over the past decade. The ease of implementation along with its benefits drew the attention of both the public and private sectors, without a doubt, the digital security industry caught on to it and consequently bloomed with more and more retail stores, shopping malls, traffic and even homes equipping themselves with closed circuit televisions (CCTV) as a means for safety precaution. With the rise of such technology and coupled with the hype around the idea of Internet of Things (IoT), Big Data and even Industry 4.0 over the recent years, there has been a deeper thirst among researchers in finding ways for practical uses of bridging existing data with technology to bring about a new wave in the research field.

\section{Research Overview}
\label{section:introduction}

The growth of the surveillance industry in conjunction with its cheap implementation cost also bring forth the rise to a constant stream of thousands of hours worth of video data filling up database warehouses across the globe by the second. While these video footages are useful when it is needed, for example when dealing with a crime scene investigation, most of the time, these data are just stored and left unprocessed, taking up additional storage space while adding to the overhead cost. 

When looking from the other side of the spectrum, other industries such as the entertainment sector are also dishing out multimedia contents at a rather alarming rate. These video files are being pushed into data warehouses and consumed by the public on a daily basis. The public sector has benefited from the rise of various video search engines such as the Google, YouTube, Dailymotion and Bing which were made available for general public. However, none of these existing technologies can be directly applied towards surveillance video footages. Clearly, the idea of using search engines to retrieve video shots is nothing new. Nevertheless, inspirations can be drawn from these technologies and applied into the research problems which will be discussed in this work. 

All of these retrieval engine technologies have one thing in common, that is, the underlying concept. In these retrieval engines, first, the process begins with extraction of video metadata such as title, description, filename, dates, subtitles and various other details that could assist in identifying these footage. Next, these videos are stored in the database along with the extracted metadata which can be used during the retrieval process. Finally, when a text-based query is issued, these video shots are retrieved and are then sorted in a way that best represents the query for the end users. 

However, as mentioned, this approach can not be directly applied towards surveillance video footage as the metadata such as title, description or even tags does not exist most of the time. Looking from the surveillance video footage standpoint, traditionally, the process of finding a particular video from a huge collection is done manually. This process involves an inquirer/user who is looking for a particular scene or shot, and an person-in-charge who manages the CCTV video collection. Now, bringing that into the scope of this work, which is a car park scenario, the inquirer would typically need to provide information such as time, date, color of the vehicle, vehicle registration number, and place of which an incident occurred. %In this work, these information are examples of what would be referred to as semantics. 

Next, the person-in-charge would have to sift through hours and hours of video in order to find a distinct video shot that matches the given semantics. This process is undeniably time consuming, laborious, monotonous and tedious. While the task is potentially simpler and straightforward when there is only one intended video shot with clear-cut definitive time and date given, this process takes an enormous effort when the time and date is not given, or even in cases when all the video shots with similar properties are desired. Evidently, there exist a gap which can be addressed using a well designed semantic extraction and retrieval engine in which this work would attempt to address.

\section{Motivation, Problem Statements and Research Questions}

As described in the previous section, the existing methods used to retrieve desired surveillance video footages are not an effective use of time and manpower. In this section, the motivation to address the topic at hand are discussed from the computer vision and natural viewpoint.

%explain what constitutes a good semantic extraction tool and a retrieval engine
%\subsection{Motivation}

At the rate of surveillance video data being collected by the second, employing human task force to manually extract information from the video data source will undoubtedly be time consuming and be extremely labour intensive. 
While not impossible, the simple act of watching the same scene over and over again from the video source will definitely put a strain on the task force. Also, when considering the amount of legacy video data that has been collected over the years and the amount of insights it could offer, the time taken to analyze them manually would be too long. Thus, affecting and foregoing the chance of reaping the benefits from these past data.
 
With the disadvantages of performing these task manually etched in mind, the use of computer vision to extract information from video data could potentially alleviate the burden. The use of such techniques is not a new idea (see Figure \ref{fig:genericCV}), works in regards to extracting vehicle-specific attributes can be found from the early 90's and it is still an ongoing research area. Looking from a wider scope, a bulk of these research has placed their focus on highway\cc{cite}, intersections\cc{cite} and dashcam\cc{cite} datasets. Among the rare few\cc{cite} that works on a car park dataset, the primary objective tend to revolve around the availability of parking spots.

\begin{figure}[!hbt]\centering
\includegraphics[width=.8\textwidth]{image/general/simpleframe.png}
\caption{Generic Computer Vision Task}
\label{fig:genericCV}
\end{figure}

Now, with the rise of technology, researchers in the computer vision area has benefited greatly as technology gets faster and at a cheaper price point. The ability to churn out metadata from raw video footages quickly combined with the advantages of easily interpretable and meaningful semantics definitely shows great potential for the commercialization of such technology. Even so, there are still plenty of challenges and opportunities to improve on existing techniques. As mentioned in Section \ref{section:introduction}, existing video retrieval engines generally relies on keyword-based queries, however, there are also works that have developed example-based queries \cc{cite}. 

Example-based queries typically require users to provide inputs in the form of an image or sample of the desired output. While it is possible to provide a thumbnail image-example for different scenes (eg: beach, mountain, waterfall) and objects (eg: bicycle, pen, lipstick), however, this is not so with surveillance video footages as these data are often similar throughout the recorded duration. 

In order to tackle the problem of interest, here are some research questions which will be addressed in this work:
\begin{enumerate}  
\item What ideology and concepts can we glean from existing techniques and take advantage of them for the sake of the topic of interest? 
\item How can we represent surveillance video footages in the form of semantics? What types of semantics can be extracted? How can these semantics be translated into something which is easily interpretable for querying?
\item How can we develop a retrieval engine which takes in input queries which are intuitive, flexible and user friendly while providing accurate search results? 
\end{enumerate}



\subsection{Research Objectives}
The objectives of this thesis can be divided into two main entities which are interdependent. As a whole, this work aims to adopt and leverage on existing frameworks to design a objects semantics \textbf{extraction} and \textbf{retrieval} framework from \textbf{car park video footages}. In order to answer the research questions posed, the following objectives were set:

\begin{enumerate}  
%\item To adopt and leverage on existing video data representation, semantics extraction and retrieval methods to design a surveillance video semantics extraction and retrieval framework.
\item To identify and extract suitable semantics which are easily interpretable while accurately describing car park surveillance scenes.
\item To design a video retrieval engine that takes in user-described queries which are intuitive while reliably providing results fast and accurately. 
\end{enumerate}

\subsection{Scope of Thesis and Expected Outcome}
\label{subsec:scope}
The work in this thesis encompasses tasks found in a generic computer vision vehicle semantic extraction and retrieval framework as illustrated in Figure \ref{fig:framework}. The preliminary task of detecting, tracking and obtaining the bounding box of each vehicle is assumed to be obtained prior to the semantic extraction task. Hence, the task of extracting semantics from the vehicles and the task of retrieving video footages are the central emphasis in this work. Here, the phrase 'long-term' is used to refer to the duration (one month) of videos in which the object semantics extracted and retrieved from. 

The scope of this work is further constrained to surveillance video footages taken from a single dataset. This dataset contains videos from a single camera with a stationary viewpoint (see Section \ref{section:dataset_used}). To further clarify the intent of this work, vehicle specific attributes are referred to as semantics. This includes vehicle color, time and date of spotting the vehicle as well as location of vehicle in the scene. However, this does not include vehicle semantics such as vehicle brand, car segment (ie: sedan, hatchback, etc), and travelling speed. The extraction of scene semantics such as weather information, lighting condition and overall car park occupancy rate are also out of the scope of this work.



\subsection{Contribution}
As this thesis comprise of a collection framework, formulation of concepts and algorithms, the contribution of this work can be divided into two main components. 

\begin{enumerate}  
\item Designed a framework for the \textbf{extraction of vehicle specific semantics} which includes color information of a vehicle throughout its tracked-life-cycle, the relative position of the vehicle at each frame, time and date information of when the vehicle was observed in the surveillance footages as well as size information of each vehicle. The proposed method employs an algorithm that averages out the dominant \textbf{color} over the course of tracking it and ranking it against a set of eleven different hues. This is a challenging task as outdoor scenarios are often non-ideal due to its ever-changing illumination and weather conditions which would directly affect the accuracy of colors for the vehicles. The relative position of each vehicle was compiled into trajectory sets that represents the \textbf{motion} of the vehicle throughout the car park. The time and date information of the vehicle are also extracted as these information are important in a surveillance scenario. To take advantage of the inherent property of video data, a spatio-temporal cube design was adopted to uniquely identify each video footage while maintaining the ability to identify them individually.  
\item Constructed a \textbf{retrieval engine} that takes in a trajectory input along with the color, time and date semantics as the input. Unlike traditional retrieval engine that takes in known-keyword-based queries, this work proposed an unconventional yet intuitive trajectory input in the form of \textbf{user-described trajectory path} on the search canvas. The color, time and date semantics are given as keyword-based inputs for the retrieval engine to locate and rank video shots according to its similarity. As with any large scale retrieval engine, the collection of all the ground truth data is not a viable option. Therefore, the performance of the proposed method was tested against semantics extracted from one month's worth of data was measured and recorded. 
\end{enumerate}



\subsection{Organization of Thesis}

The road-map of this thesis is laid out as follow: First, the introduction on the subject matter is discussed in this chapter. Next, related theoretical background concepts are described in Section \ref{subsec:relatedConcepts}, then related works in this research field are reviewed thoroughly to provide an idea what has been tried and solved, along with areas of improvements that could be addressed. This is then followed with a chapter that describes the overview of the proposed framework in Section \ref{section:framework}. Upon which, the proposed video semantics extraction methods for both the vehicle color and vehicle motion will be discussed. Next, the proposed retrieval engine along with its results are reported in Chapter \ref{section:retrievalengine}, this chapter also includes some analysis of the results based on the proposed methods in this thesis. Finally, the conclusion of this work along with a list of suggestions for future works is prepared in Chapter \ref{section:conclusion}. 
